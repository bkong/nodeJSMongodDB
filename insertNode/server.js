var express = require('express');
var server = express();

var client = require('mongodb').MongoClient;
var assert = require('assert');

var url = "mongodb://localhost:27017/test";

server.get('/', function (req,res){
	res.sendFile(__dirname+'/index1.html');
});

server.get('/index', function (req,res){

	var data = {FirstName:req.query.fname, LastName:req.query.lname};
	client.connect(url,function (err,db){
		assert.equal(null, err);
		console.log("Connected correctly to server");
		db.collection('collection').insertOne(data, function (err,res){
			assert.equal(null,err);
			console.log('data INSERTED.');
			db.close();
		});
	});
	res.sendFile(__dirname+'/index1.html');
});


server.listen(3030,console.log("Connected to port 3030"));